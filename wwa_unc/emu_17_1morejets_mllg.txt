imax 1   number of channels
jmax *   number of processes-1
kmax *  number of nuisance parameters (sources of systematical uncertainties)
shapes * * root/wwg_emu2017_1morejets_input.root hist_2Dfit_$PROCESS hist_2Dfit_$PROCESS_$SYSTEMATIC
shapes data_obs * root/wwg_emu2017_1morejets_input.root  hist_2Dfit_Data
Observation 290.00000
# we have just one channel, in which we observe 0 events
bin emu17_1morejets
# now we list the expected events for signal and all backgrounds in that bin
# the second process line must have a positive number for backgrounds, and 0 for signal
# then we list the independent sources of uncertainties, and give their effect (syst. error)
# on each process and bin
bin	emu17_1morejets	emu17_1morejets	emu17_1morejets	emu17_1morejets	emu17_1morejets	emu17_1morejets
process	Sig	VA	plj	fakeL	VV	Top
process	0	1	2	3	4	5
rate	30.721	30.914	94.202	21.364	6.740	73.112
lumi_cor	lnN	1.009	1.009	-	-	1.009	1.009
pileup	 shape	1.000	1.000	-	-	1.000	1.000
btag_bc_corr	 shape	1.000	1.000	-	-	1.000	1.000
btag_light_corr	 shape	1.000	1.000	-	-	1.000	1.000
l1pref	 shape	1.000	1.000	-	-	1.000	1.000
muon_id	 shape	1.000	1.000	-	-	1.000	1.000
ele_reco	 shape	1.000	1.000	-	-	1.000	1.000
ele_id	 shape	1.000	1.000	-	-	1.000	1.000
photon_id	 shape	1.000	1.000	-	-	1.000	1.000
WWG_emu_pdf	shape	1.000	-	-	-	-	-
VA_pdf	shape	-	1.000	-	-	-	-
VV_pdf	shape	-	-	-	-	1.000	-
Top_pdf	shape	-	-	-	-	-	1.000
WWG_emu_PSWeight	shape	1.000	-	-	-	-	-
VA_PSWeight	shape	-	1.000	-	-	-	-
VV_PSWeight	shape	-	-	-	-	1.000	-
Top_PSWeight	shape	-	-	-	-	-	1.000
WWG_emu_scale	shape	1.000	-	-	-	-	-
VA_scale	shape	-	1.000	-	-	-	-
VV_scale	shape	-	-	-	-	1.000	-
Top_scale	shape	-	-	-	-	-	1.000
theory group = WWG_emu_pdf	VA_pdf	VV_pdf	Top_pdf	WWG_emu_PSWeight	VA_PSWeight	VV_PSWeight	Top_PSWeight	WWG_emu_scale	VA_scale	VV_scale	Top_scale
lumi_uncor17	lnN	1.020	1.020	-	-	1.020	1.020
jesTotal17	 shape	1.000	1.000	-	-	1.000	1.000
HLT_SF17	 shape	1.000	1.000	-	-	1.000	1.000
btag_bc_uncorr17	 shape	1.000	1.000	-	-	1.000	1.000
btag_light_uncorr17	 shape	1.000	1.000	-	-	1.000	1.000
unclustEn17	 shape	1.000	1.000	-	-	1.000	1.000
fake_electron17	 shape	-	-	-	1.000	-	-
fake_muon17	 shape	-	-	-	1.000	-	-
fakephoton17	 shape	-	-	1.000	-	-	-
Top_rate	rateParam	*	Top	1.0	[0.1,4]
emu17_1morejets	autoMCStats	10	0	1 
 
luminosity group =  lumi_cor lumi_uncor17 
pu group =  pileup 
muon group =  muon_id 
Toprate group =  Top_rate 
HLTSF group =  HLT_SF17 
JESR group = jesTotal17 
egamma group =  ele_reco ele_id photon_id 
fake group =  fake_electron17 fake_muon17 fakephoton17 
pref group =  l1pref 
btag group =  btag_bc_corr btag_bc_uncorr17 btag_light_corr btag_light_uncorr17 
