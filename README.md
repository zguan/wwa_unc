# SMP-22-006 uncertainties calculations

## Introduction

SMP-22-006(WWgamma electron-muon channel) designed different categories for SR and CR. when we calculated the uncertainties in different regions, the correlations in bins and processes should be included.

this repositories will introduce how to prepare the datacards based on the histograms and then calculated the syst+stat uncertainties separately.


The analysis introductions can be found in:

https://cms.cern.ch/iCMS/analysisadmin/cadilines?line=SMP-22-006&tp=an&id=2562&ancode=SMP-22-006

## Environment

mainly based on Higgs Combine Tool.

```export SCRAM_ARCH=slc7_amd64_gcc700
cmsrel CMSSW_10_2_13
cd CMSSW_10_2_13/src
cmsenv
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
cd HiggsAnalysis/CombinedLimit
scram b -j 10
```

## Steps for Uncertainties

### Step1: prepare data cards
```
python creat_datacards.py 
```
combine the datacards:
```
combineCards.py emu*.txt >all.txt
text2workspace.py all.txt -m 125
% combine -M Significance --expectSignal=1 -t -1 > result_all_expected.txt
% combine -M Significance  > result_all_expected.txt
```
### Step2: prepare prefit and postfit hists and covariance matrices

```
combine -M FitDiagnostics all.txt --saveShapes --saveWithUncertainties --saveNormalizations —saveOverallShapes
```

#### covariance matrices:

#### 1. prefit:
```
PostFitShapesFromWorkspace -w all.root all.txt --o hists.root --postfit -f fitDiagnosticsTest.root:nuisances_prefit_res --total-shapes --covariance 1 --skip-prefit --skip-proc-errs --print 1 
```

#### 2. postfit:
```
PostFitShapesFromWorkspace -w all.root all.txt --o hists.root --postfit -f fitDiagnosticsTest.root:fit_s --total-shapes --covariance 1 --skip-prefit --skip-proc-errs --print 1 
```

then the covariances matrix can be found in the output root file.
### Step3: calculate the uncertainties based on covariance matrices

the uncertainty calculation details can be found in slides[1]

The main formula is

![2](https://latex.codecogs.com/svg.image?\sigma_f^2=\sum_i^n&space;&space;\sigma_{ii}^2&plus;\sum_i^n&space;\sum_{j(j&space;\neq&space;i)}^n&space;\sigma_{ij}^2&space;)

the uncertainty in every bin can be calculated with:
```
root -l get_hist.C
```

the uncertainties for every sig/bkg processes can be calculated with:
```
root -l check_single_unc.C
```



