from ROOT import gROOT, THStack, TH1D, TList, TFile
import sys
from numpy import sum
#process=['Sig','VA','plj','fakeL','VV','Top','Sig_out']
process=['Sig','VA','plj','fakeL','VV','Top']  # part of hist name
NPs_corr=['lumi_cor','pileup','btag_bc_corr','btag_light_corr','l1pref','muon_id','ele_reco','ele_id','photon_id','pdf','PSWeight','scale'] # syst uncs name
NPs_uncorr=['lumi_uncor','jesTotal','HLT_SF','btag_bc_uncorr','btag_light_uncorr','unclustEn','fake_electron','fake_muon','fakephoton'] # syst uncs name
lumi_uncorr={'16':1.010,'17':1.020,'18':1.015} # lumi unc value
lumi_corr={'16':1.006,'17':1.009,'18':1.020} # lumi unc value

def fill_line(year,process,NPs,channel):
	f_input = TFile.Open('root/wwg_emu20'+year+'_'+channel+'_input.root')
	th1_Data=f_input.Get('hist_2Dfit_Data')
        th1_MC=[]
	for i in process:
	    th1_MC.append(f_input.Get('hist_2Dfit_'+i))
	f = open('./emu_%s_%s_mllg.txt'%(year,channel),'w')
	f.write('imax 1   number of channels\n')
	f.write('jmax *   number of processes-1\n')
	f.write('kmax *  number of nuisance parameters (sources of systematical uncertainties)\n')
	f.write('shapes * * root/wwg_emu20'+year+'_'+channel+'_input.root hist_2Dfit_$PROCESS hist_2Dfit_$PROCESS_$SYSTEMATIC\n')
	f.write('shapes data_obs * root/wwg_emu20'+year+'_'+channel+'_input.root  hist_2Dfit_Data\n')
        f.write('Observation %0.5f\n'%(th1_Data.GetSum()))
        f.write('# we have just one channel, in which we observe 0 events\n')
        f.write('bin emu'+year+'_'+channel+'\n')
	f.write('# now we list the expected events for signal and all backgrounds in that bin\n')
	f.write('# the second process line must have a positive number for backgrounds, and 0 for signal\n')
	f.write('# then we list the independent sources of uncertainties, and give their effect (syst. error)\n')
	f.write('# on each process and bin\n')
	f.write('bin\t')
        for i in range(0,len(process)):
            f.write('emu'+year+'_'+channel+'\t') if i!=len(process)-1 else f.write('emu'+year+'_'+channel+'\n')

	f.write('process\t')
        for i in range(0,len(process)):
	    f.write(process[i]+'\t') if i!=len(process)-1 else f.write(process[i]+'\n')

	f.write('process\t')
        for i in range(0,len(process)):
            f.write('{}\t'.format(i)) if i!=len(process)-1 else f.write('{}\n'.format(i))

	f.write('rate\t')
        for i in range(0,len(process)):
            f.write('%0.3f\t'%(th1_MC[i].GetSum())) if i!=len(process)-1 else f.write('%0.3f\n'%(th1_MC[i].GetSum()))
        
	theory=[]
        for np in NPs_corr:
            if 'l1pref' in np and '18' in year:
                continue
            if ('lumi' not in np) and ('PS' not in np) and ('scale' not in np) and ('pdf' not in np):
                f.write(np+'\t shape\t') 
                for i in range(0,len(process)):
                    if 'plj' in process[i] or 'fake' in process[i]:
                        f.write('-\t')
		    else:
                        f.write('%.3f\t'%(1)) if i!=len(process)-1 else f.write('%.3f\n'%(1))
            elif ('PS' in np) or ('scale' in np) or ('pdf' in np):
                for i in range(0,len(process)):
                    if 'plj' in process[i] or 'fake' in process[i]:
                        continue
		    if 'Sig' not in process[i]:
                        f.write(process[i]+'_'+np+'\tshape\t') 
                        theory.append(process[i]+'_'+np)
                    else:
			f.write('WWG_emu_'+np+'\tshape\t') 
                        theory.append('WWG_emu_'+np)
                    for k in range(0,i):
                        f.write('-\t') if k!=len(process)-1 else f.write('-\n')
                    f.write('%.3f\t'%(1)) if i!=len(process)-1 else f.write('%.3f\n'%(1))
                    for k in range(i+1,len(process)):
                        f.write('-\t') if k!=len(process)-1 else f.write('-\n')
            elif 'lumi' in np:
                  f.write(np+'\tlnN\t')
                  for i in range(0,len(process)):
                    if 'plj' in process[i] or 'fake' in process[i]:
                        f.write('-\t')
		    else:
                        f.write('%.3f\t'%(lumi_corr[year])) if i!=len(process)-1 else f.write('%.3f\n'%(lumi_corr[year]))
        f.write('theory group = ') 
        for i in range(0,len(theory)):
            f.write(theory[i]+'\t') if i!=len(theory)-1 else f.write(theory[i]+'\n')
        for np in NPs_uncorr:
            if ('lumi' not in np) and ('fake' not in np):
                f.write(np+year+'\t shape\t') 
                for i in range(0,len(process)):
                    if 'plj' in process[i] or 'fake' in process[i]:
                        f.write('-\t')
		    else:
                        f.write('%.3f\t'%(1)) if i!=len(process)-1 else f.write('%.3f\n'%(1))
            elif 'lumi' in np:
                  if 'lumi_cor' in np: f.write(np+'\tlnN\t')
                  if 'lumi_uncor' in np: f.write(np+year+'\tlnN\t')
                  for i in range(0,len(process)):
                    if 'plj' in process[i] or 'fake' in process[i]:
                        f.write('-\t')
		    else:
                        f.write('%.3f\t'%(lumi_uncorr[year])) if i!=len(process)-1 else f.write('%.3f\n'%(lumi_uncorr[year]))
            elif 'fake' in np:
                  f.write(np+year+'\t shape\t') 
                  for i in range(0,len(process)):
                      if 'photon' in np and 'plj' not in process[i]:                   
                          f.write('-\t') if i!=len(process)-1 else f.write('-\n')
                      elif (('electron' in np) or ('muon' in np)) and 'fake' not in process[i]:                   
                          f.write('-\t') if i!=len(process)-1 else f.write('-\n')
                      else:
			  f.write('%.3f\t'%(1)) if i!=len(process)-1 else f.write('%.3f\n'%(1))     
                
        f.write('Top_rate\trateParam\t*\tTop\t1.0\t[0.1,4]\n')
        f.write('emu'+year+'_'+channel+'\tautoMCStats\t10\t0\t1')
        f.write(' \n')
        f.write(' \n')
        f.write('luminosity group = ' + ' lumi_cor'+ ' lumi_uncor'+year +' \n')
        f.write('pu group = ' + ' pileup' +' \n')
        f.write('muon group = ' + ' muon_id'+' \n')
        f.write('Toprate group = ' + ' Top_rate'+' \n')
        f.write('HLTSF group = ' + ' HLT_SF'+year+' \n')
        f.write('JESR group = ' + 'jesTotal'+year +' \n')
        f.write('egamma group = ' + ' ele_reco'+' ele_id'+' photon_id'+' \n')
        f.write('fake group = ' + ' fake_electron'+year +' fake_muon'+year+' fakephoton'+year+' \n')
        if '16' in year or '17' in year: f.write('pref group = ' + ' l1pref'+' \n')
        f.write('btag group = ' + ' btag_bc_corr'+' btag_bc_uncorr'+year+' btag_light_corr'+' btag_light_uncorr'+year+' \n')


year=['16','17','18']
channel=['0jets','1morejets','SSWWA_alljets','Top_alljets']
NPs=NPs_corr+NPs_uncorr
for y in year:
   for c in channel:
       fill_line(y,process,NPs,c)
